<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel-Basic</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
    <a href="{{ route('index') }}" class="btn btn-success">< Back</a>
    <div class="mb-3">
        <form action="{{ route('store') }}" method="post"> 
        @csrf
            <label for="formGroupExampleInput" class="form-label">Name</label>
            <input type="text" class="form-control" id="formGroupExampleInput" name="name" placeholder="Name">
            <label for="formGroupExampleInput" class="form-label">Email</label>
            <input type="text" class="form-control" id="formGroupExampleInput" name="email" placeholder="Email">
            <label for="formGroupExampleInput" class="form-label">Password</label>
            <input type="text" class="form-control" id="formGroupExampleInput" name="password" placeholder="Password">
            <input type="submit" name="submit" class="btn btn-success" value="Create">
        </form>
    </div>
</body>
</html>