<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', 'API\UserController@index')->name('index');
Route::post('/store', 'API\UserController@store')->name('store');
Route::get('/edit/{id}', 'API\UserController@edit')->name('edit');
Route::post('/update/{id}', 'API\UserController@update')->name('update');
Route::get('/destroy/{id}', 'API\UserController@destroy')->name('destroy');