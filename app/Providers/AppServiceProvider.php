<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// use App\Repositories\UserRepository;
use App\Repositories\UserInterface;
// use App\Repositories\ApiUserRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserRepository::class, ApiUserRepository::class, UserInterface::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // 
    }
}
