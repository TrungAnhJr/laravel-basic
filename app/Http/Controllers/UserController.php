<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    public function __construct(UserRepository $user) 
    {
        $this->user = $user;
    }
    
    public function index()
    {
        $users = $this->user->all();

        return view('crud.index', compact('users'));
    }

    public function create()
    {
        return view('crud.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required'
        ]);
        $this->user->store($request->all());
            
        return redirect()->route('index');
    }

    public function edit($id)
    {
        $user = $this->user->get($id);

        return view('crud.edit', compact('user'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);
        $this->user->update($id, $request->all());

        return redirect()->route('index');
    }

    public function destroy($id)
    {
        $this->user->delete($id);

        return redirect()->route('index');
    }
}
