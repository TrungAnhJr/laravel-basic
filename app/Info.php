<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected $table = 'infos';
    protected $primarykey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'name',
        'email',
    ];
}
